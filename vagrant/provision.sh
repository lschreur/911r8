#!/bin/bash

ENV=DEV
DBNAME=foo
DBUSER=foo
DBPASS=foo
DBHOST=localhost

APACHE_USER=ubuntu

# Add swap space.
sh /vagrant/vagrant/swap.sh

# Set the mysql password so we're not asked for it during provisioning.
debconf-set-selections <<< "mysql-server mysql-server/root_password password ${DBPASS}"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${DBPASS}"

# Add PHP repos.
add-apt-repository ppa:ondrej/php

# Install prep.
apt-get update
#apt-get -y upgrade

# Install basic necessities.
apt-get -y install ntp git htop tree zip unzip python-simplejson
systemctl restart ntp.service

# Install php.
PHP_VERSION="7.1"

apt-get -y install \
  libapache2-mod-php${PHP_VERSION} \
  php${PHP_VERSION} \
  php${PHP_VERSION}-cli \
  php${PHP_VERSION}-curl \
  php${PHP_VERSION}-mcrypt \
  php${PHP_VERSION}-intl \
  php${PHP_VERSION}-json \
  php${PHP_VERSION}-xml \
  php${PHP_VERSION}-mbstring \
  php${PHP_VERSION}-mysql \
  php${PHP_VERSION}-bcmath \
  php${PHP_VERSION}-dev \
  php-pear

sed -i "s/;date timezone =/date timezone = UTC/" /etc/php/${PHP_VERSION}/apache2/php.ini
sed -i "s/;date timezone =/date timezone = UTC/" /etc/php/${PHP_VERSION}/cli/php.ini

sed -i "s/;error_log = syslog/error_log = syslog/" /etc/php/${PHP_VERSION}/apache2/php.ini
sed -i "s/;error_log = syslog/error_log = syslog/" /etc/php/${PHP_VERSION}/cli/php.ini

# Install Apache
apt-get -y install apache2

cat > /etc/apache2/sites-available/000-default.conf <<EOL
<VirtualHost *:80>
  #ServerName www.example.com

  ServerAdmin webmaster@localhost
  DocumentRoot /vagrant/public

  <Directory /vagrant/public>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order allow,deny
    Allow from all
    Require all granted
  </Directory>

  ErrorLog \${APACHE_LOG_DIR}/error.log
  CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOL

sed -i "s/User \${APACHE_RUN_USER}/User ${APACHE_USER}/" /etc/apache2/apache2.conf
sed -i "s/Group \${APACHE_RUN_GROUP}/Group ${APACHE_USER}/" /etc/apache2/apache2.conf

# Enable required Apache modules and restart server.
a2enmod rewrite
a2enmod php${PHP_VERSION}
a2enmod status
a2dismod mpm_event
a2enmod mpm_prefork
systemctl restart apache2.service

# Install and configure mysql.
apt-get -y install mysql-server
#sed -i "s/bind-address$(printf '\t\t')= 127.0.0.1/bind-address$(printf '\t\t')= 0.0.0.0/" /etc/mysql/my.cnf
systemctl restart mysql.service

# Create the dev and prod database with user. The dev database is for development, the prod
# database can be used to create/test migrations against.
declare -a dbs=("dev" "prod")
for i in "${dbs[@]}"
do
cat > /tmp/${DBNAME}_${i}.sql <<EOL
create database if not exists ${DBNAME}_${i};
alter database ${DBNAME}_${i} character set utf8 collate utf8_general_ci;
grant usage on ${DBNAME}_${i}.* to '${DBUSER}'@'%' identified by '${DBPASS}';
grant all privileges on ${DBNAME}_${i}.* to '${DBUSER}'@'%'; 
flush privileges;
EOL
mysql -uroot -p${DBPASS} -h${DBHOST} < /tmp/${DBNAME}_${i}.sql
done

# Install Composer.
php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/bin/composer

# Install PHP fixer.
curl -L https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases/download/v2.0.0/php-cs-fixer.phar -o php-cs-fixer
mv php-cs-fixer /usr/bin/php-cs-fixer
chmod a+x /usr/bin/php-cs-fixer


sudo -H -u ubuntu bash -c 'cd /vagrant; composer install'
sudo -H -u ubuntu bash -c 'cd /vagrant; composer dump-autoload'
sudo -H -u ubuntu bash -c 'cd /vagrant; php artisan migrate:fresh --seed'
