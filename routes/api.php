<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::prefix('v1')->group(function () {
    Route::get('/cars/{carId}/waitlists/{waitlistId}', 'WaitlistController@view');
    Route::delete('/cars/{carId}/waitlists/{waitlistId}', 'WaitlistController@delete');
    Route::put('/cars/{carId}/waitlists/{waitlistId}', 'WaitlistController@update');
    Route::post('/cars/{carId}/waitlists', 'WaitlistController@create');
    Route::get('/cars/{carId}/waitlists', 'WaitlistController@index');

    Route::get('/cars/{carId}/suppliers', 'SupplierController@index');
    
    Route::get('/cars/{carId}/colors', 'ColorController@index');

    Route::get('/cars/{carId}', 'CarController@view');
    Route::get('/cars', 'CarController@index');

    Route::post('/customers', 'CustomerController@create');
    Route::get('/customers', 'CustomerController@index');

    Route::get('/suppliers', 'SupplierController@index');

    Route::get('/colors', 'ColorController@index');
});
