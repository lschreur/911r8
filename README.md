# How to

To run this demonstration build the VM with Vagrant (and VirtualBox):

    vagrant up

After the VM has been build, log into it:

    vagrant ssh

Alternatively, the VM is accessible from the outside through `localhost:5008` rather then just `localhost`.

# Accessing the API

Let's start by displaying all the cars:

    curl -s http://localhost/api/v1/cars | python -m json.tool

As you can see, the 911-R8 is in "development".

Now let's get a list of the available colors:

    curl -s http://localhost/api/v1/colors | python -m json.tool

And a list of the tire suppliers who have already commited:

    curl -s http://localhost/api/v1/suppliers | python -m json.tool

Great.

Let's create a customer named "Bob":

    curl -s -H "Content-Type: application/json" -X POST -d '{"name":"Bob"}' http://localhost/api/v1/customers | python -m json.tool

Let's place Bob on the wait list for the Car with Id 1 (911-R8):

    curl -s -H "Content-Type: application/json" -X POST -d '{"customer_id":1,"color_id":4,"supplier_id":3}' http://localhost/api/v1/cars/1/waitlists | python -m json.tool

Display all the wait lists for the 911-R8:

    curl -s http://localhost/api/v1/cars/1/waitlists | python -m json.tool

We can update our wait list entry to change our color and/or tire supplier:

    curl -s -H "Content-Type: application/json" -X PUT -d '{"color_id":1,"supplier_id":2}' http://localhost/api/v1/cars/1/waitlists/1 | python -m json.tool

Delete our wait list entry:

    curl -s -X DELETE http://localhost/api/v1/cars/1/waitlists/1 | python -m json.tool

And we're done...
