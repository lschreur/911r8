<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Core\Service\Car\CarService;

class CarController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $carServices;

    public function __construct(CarService $carService)
    {
        $this->carService = $carService;
    }

    public function index()
    {
        $cars = \App\Core\Model\Car::all();

        $result = [
            'cars' => \App\Mapping\CarCollectionMapping::map($cars),
        ];

        return response()->json($result);
    }

    public function view(string $carId)
    {
        $car = $this->carService->getById((int)$carId);

        $result = [
            'car' => \App\Mapping\CarMapping::map($car),
        ];

        return response()->json($result);
    }    
}
