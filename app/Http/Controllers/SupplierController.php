<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class SupplierController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $suppliers = \App\Core\Model\Supplier::all();

        $result = [
            'suppliers' => \App\Mapping\SupplierCollectionMapping::map($suppliers),
        ];

        return response()->json($result);
    }
}
