<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;

use App\Core\Service\Customer\CustomerService;

class CustomerController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    public function index()
    {
        $customers = \App\Core\Model\Customer::all();

        $result = [
            'customers' => \App\Mapping\CustomerCollectionMapping::map($customers),
        ];

        return response()->json($result);
    }

    public function create(Request $request)
    {
        $result = null;
        $status = 201;

        try {
            $data = $request->json()->all();

            if (!isset($data['name'])) {
                throw new Exceptions\RequestException("The 'name' must be specified.");
            }

            $name = array_get($data, 'name');

            try {
                $customer = $this->customerService->create($name);
            }
            catch (\App\Core\CoreException $e) {
                throw new Exceptions\RequestException($e->getMessage());                
            }

            $result = [
                'customer' => \App\Mapping\CustomerMapping::map($customer),
            ];
        }
        catch (Exceptions\RequestException $e) {
            $status = $e->getCode();
            $result = [
                'message' => $e->getMessage(),
            ];
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            $status = 500;
            $result = [
                'message' => "Internal Server Error",
            ];
        }

        return response()->json($result, $status);
    }    
}
