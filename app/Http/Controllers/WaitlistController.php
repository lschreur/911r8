<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;

use App\Core\Service\Car\CarService;
use App\Core\Service\Color\ColorService;
use App\Core\Service\Supplier\SupplierService;
use App\Core\Service\Customer\CustomerService;
use App\Core\Service\Waitlist\WaitlistService;

class WaitlistController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $carService;
    protected $waitlistService;

    // The reason we perfer "setter" injection...
    public function __construct(CarService $carService, ColorService $colorService, SupplierService $supplierService, CustomerService $customerService, WaitlistService $waitlistService)
    {
        $this->carService = $carService;
        $this->colorService = $colorService;
        $this->supplierService = $supplierService;
        $this->customerService = $customerService;
        $this->waitlistService = $waitlistService;
    }

    public function index(Request $request, $carId)
    {
        $result = null;
        $status = 200;

        try {
            try {
                $car = $this->carService->getById((int)$carId);

                $waitlists = $this->waitlistService->getAllByCar($car);
            }
            catch (\App\Core\CoreException $e) {
                throw new Exceptions\RequestException($e->getMessage());                
            }

            $result = [
                'waitlists' => \App\Mapping\WaitlistCollectionMapping::map($waitlists),
            ];
        }
        catch (\Exceptions\RequestException $e) {
            $status = $e->getCode();
            $result = [
                'message' => $e->getMessage(),
            ];
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            $status = 500;
            $result = [
                'message' => "Internal Server Error",
            ];
        }

        return response()->json($result);
    }

    public function view(Request $request, $carId, $waitlistId)
    {
        $result = null;
        $status = 200;

        try {
            try {
                $waitlist = $this->waitlistService->getById((int)$waitlistId);
            }
            catch (\App\Core\CoreException $e) {
                throw new Exceptions\RequestException($e->getMessage());                
            }

            $result = [
                'waitlist' => \App\Mapping\WaitlistMapping::map($waitlist),
            ];
        }
        catch (\Exceptions\RequestException $e) {
            $status = $e->getCode();
            $result = [
                'message' => $e->getMessage(),
            ];
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            $status = 500;
            $result = [
                'message' => "Internal Server Error",
            ];
        }

        return response()->json($result);
    }

    public function create(Request $request, $carId)
    {
        $result = null;
        $status = 201;

        try {
            $data = $request->json()->all();

            if (!isset($data['customer_id'])) {
                throw new Exceptions\RequestException("The 'customer_id' must be specified.");
            }

            $customerId = (int)array_get($data, 'customer_id');
            $colorId = array_get($data, 'color_id');
            $supplierId = array_get($data, 'supplier_id');

            try {
                $car = $this->carService->getById((int)$carId);

                $customer = $this->customerService->getById($customerId);

                $color = empty($colorId) ? null : $this->colorService->getById((int)$colorId);

                $supplier = empty($supplierId) ? null : $this->supplierService->getById((int)$supplierId);

                $waitlist = $this->waitlistService->create($car, $customer, $color, $supplier);
            }
            catch (\App\Core\CoreException $e) {
                throw new Exceptions\RequestException($e->getMessage());                
            }

            $result = [
                'waitlist' => \App\Mapping\WaitlistMapping::map($waitlist),
            ];
        }
        catch (Exceptions\RequestException $e) {
            $status = $e->getCode();
            $result = [
                'message' => $e->getMessage(),
            ];
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            $status = 500;
            $result = [
                'message' => "Internal Server Error",
            ];
        }

        return response()->json($result, $status);
    }

    public function update(Request $request, $carId, $waitlistId)
    {
        $result = null;
        $status = 200;

        try {
            $data = $request->json()->all();

            $colorId = array_get($data, 'color_id');
            $supplierId = array_get($data, 'supplier_id');

            try {
                $color = empty($colorId) ? null : $this->colorService->getById((int)$colorId);

                $supplier = empty($supplierId) ? null : $this->supplierService->getById((int)$supplierId);

                $waitlist = $this->waitlistService->getById((int)$waitlistId);
                $waitlist = $this->waitlistService->update($waitlist, $color, $supplier);
            }
            catch (\App\Core\CoreException $e) {
                throw new Exceptions\RequestException($e->getMessage());                
            }

            $result = [
                'waitlist' => \App\Mapping\WaitlistMapping::map($waitlist),
            ];
        }
        catch (Exceptions\RequestException $e) {
            $status = $e->getCode();
            $result = [
                'message' => $e->getMessage(),
            ];
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            $status = 500;
            $result = [
                'message' => "Internal Server Error",
            ];
        }

        return response()->json($result, $status);
    }

    public function delete(Request $request, $carId, $waitlistId)
    {
        $result = null;
        $status = 200;

        try {
            $data = $request->json()->all();

            try {
                $waitlist = $this->waitlistService->getById((int)$waitlistId);

                $this->waitlistService->delete($waitlist);
            }
            catch (\App\Core\CoreException $e) {
                throw new Exceptions\RequestException($e->getMessage());                
            }
        }
        catch (Exceptions\RequestException $e) {
            $status = $e->getCode();
            $result = [
                'message' => $e->getMessage(),
            ];
        }
        catch (\Exception $e) {
            Log::error($e->getMessage());

            $status = 500;
            $result = [
                'message' => "Internal Server Error",
            ];
        }

        return response()->json($result, $status);
    }
}
