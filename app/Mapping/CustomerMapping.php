<?php

namespace App\Mapping;

class CustomerMapping
{
    public static function map(\App\Core\Model\Customer $customer) : array
    {
        return [
            'id' => $customer->id,
            'name' => $customer->name,
            //'created_at' => $customer->created_at,
            //'updated_at' => $customer->updated_at,
        ];
    }
}
