<?php

namespace App\Mapping;

class ColorMapping
{
    public static function map(\App\Core\Model\Color $color) : array
    {
        return [
            'id' => $color->id,
            'name' => $color->name,
            //'created_at' => $color->created_at,
            //'updated_at' => $color->updated_at,
        ];
    }
}
