<?php

namespace App\Mapping;

class ColorCollectionMapping
{
    public static function map($colors) : array
    {
        $result = [];

        foreach ($colors as $color) {
            $result[] = \App\Mapping\ColorMapping::map($color);
        }

        return $result;
    }
}
