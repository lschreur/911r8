<?php

namespace App\Mapping;

class CustomerCollectionMapping
{
    public static function map($customers) : array
    {
        $result = [];

        foreach ($customers as $customer) {
            $result[] = \App\Mapping\CustomerMapping::map($customer);
        }

        return $result;
    }
}
