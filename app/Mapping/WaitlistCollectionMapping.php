<?php

namespace App\Mapping;

class WaitlistCollectionMapping
{
    public static function map($waitlists) : array
    {
        $result = [];

        foreach ($waitlists as $waitlist) {
            $result[] = \App\Mapping\WaitlistMapping::map($waitlist);
        }

        return $result;
    }
}
