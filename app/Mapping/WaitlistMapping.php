<?php

namespace App\Mapping;

class WaitlistMapping
{
    public static function map(\App\Core\Model\Waitlist $waitlist) : array
    {
        return [
            'id' => $waitlist->id,
            'car' => \App\Mapping\CarMapping::map($waitlist->car),
            'customer' => \App\Mapping\CustomerMapping::map($waitlist->customer),
            'color' => empty($waitlist->color) ? null : \App\Mapping\ColorMapping::map($waitlist->color),
            'supplier' => empty($waitlist->supplier) ? null : \App\Mapping\SupplierMapping::map($waitlist->supplier),
            //'created_at' => $waitlist->created_at,
            //'updated_at' => $waitlist->updated_at,
        ];
    }
}
