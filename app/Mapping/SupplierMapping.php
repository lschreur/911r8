<?php

namespace App\Mapping;

class SupplierMapping
{
    public static function map(\App\Core\Model\Supplier $supplier) : array
    {
        return [
            'id' => $supplier->id,
            'name' => $supplier->name,
            //'created_at' => $supplier->created_at,
            //'updated_at' => $supplier->updated_at,
        ];
    }
}
