<?php

namespace App\Mapping;

class CarCollectionMapping
{
    public static function map($cars) : array
    {
        $result = [];

        foreach ($cars as $car) {
            $result[] = \App\Mapping\CarMapping::map($car);
        }

        return $result;
    }
}
