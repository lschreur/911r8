<?php

namespace App\Mapping;

class CarMapping
{
    public static function map(\App\Core\Model\Car $car) : array
    {
        return [
            'id' => $car->id,
            'name' => $car->name,
            'status' => $car->status,
            //'created_at' => $car->created_at,
            //'updated_at' => $car->updated_at,
        ];
    }
}
