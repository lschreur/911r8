<?php

namespace App\Mapping;

class SupplierCollectionMapping
{
    public static function map($suppliers) : array
    {
        $result = [];

        foreach ($suppliers as $supplier) {
            $result[] = \App\Mapping\SupplierMapping::map($supplier);
        }

        return $result;
    }
}
