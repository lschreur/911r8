<?php

use Illuminate\Database\Seeder;

use App\Core\Model\Color;
use App\Core\Model\Customer;
use App\Core\Model\Supplier;
use App\Core\Model\Waitlist;

class WaitlistTableSeeder extends Seeder {

    public function run()
    {
        DB::table('waitlists')->delete();

        $car = \App\Core\Model\Car::where('name', '911-R8')->first();

        Waitlist::create([
            'car_id'      => $car->id,
            'customer_id' => Customer::where('name', 'Luke')->first()->id,
            'color_id'    => Color::where('name', 'Not Gold Silver')->first()->id,
            'supplier_id' => Supplier::where('name', 'Burn Out Central')->first()->id,
        ]);
    }

}
