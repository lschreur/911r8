<?php

use Illuminate\Database\Seeder;

use App\Core\Model\Car;

class CarTableSeeder extends Seeder {

    public function run()
    {
        DB::table('cars')->delete();

        Car::create([
            'name' => '911-R8',
            'status' => Car::STATUS_DEVELOPMENT,
        ]);
    }

}
