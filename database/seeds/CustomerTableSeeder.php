<?php

use Illuminate\Database\Seeder;

use App\Core\Model\Customer;

class CustomerTableSeeder extends Seeder {

    public function run()
    {
        DB::table('customers')->delete();

        Customer::create(['name' => 'Luke']);
    }

}
