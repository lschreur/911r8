<?php

use Illuminate\Database\Seeder;

use App\Core\Model\Supplier;

class SupplierTableSeeder extends Seeder {

    public function run()
    {
        DB::table('suppliers')->delete();

        Supplier::create(['name' => "Tire's R Us"]);
        Supplier::create(['name' => "Black Rubber"]);
        Supplier::create(['name' => "Burn Out Central"]);
    }

}
