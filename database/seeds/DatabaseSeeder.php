<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ColorTableSeeder::class);
        $this->call(CarTableSeeder::class);
        $this->call(SupplierTableSeeder::class);
        //$this->call(CustomerTableSeeder::class);
        //$this->call(WaitlistTableSeeder::class);
    }
}
