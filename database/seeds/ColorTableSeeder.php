<?php

use Illuminate\Database\Seeder;

use App\Core\Model\Color;

class ColorTableSeeder extends Seeder {

    public function run()
    {
        DB::table('colors')->delete();

        Color::create(['name' => 'Photon Blue']);
        Color::create(['name' => 'Nuclear Red']);
        Color::create(['name' => 'Black Hole Black']);
        Color::create(['name' => 'Not Gold Silver']);
        Color::create(['name' => 'Galaxy Purple']);
    }

}
