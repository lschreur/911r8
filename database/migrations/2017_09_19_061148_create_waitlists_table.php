<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaitlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waitlists', function (Blueprint $table) {
            $table->increments('id');
            $table
                ->integer('car_id')
                ->unsigned()
                ->foreign('car_id')
                ->references('id')
                ->on('cars')
            ;
            $table
                ->integer('customer_id')
                ->unsigned()
                ->foreign('customer_id')
                ->references('id')
                ->on('customers')
            ;
            $table
                ->integer('color_id')
                ->unsigned()
                ->foreign('color_id')
                ->references('id')
                ->on('colors')
                ->nullable()                // <-- Nullable. Color can be changed.
            ;
            $table
                ->integer('supplier_id')
                ->unsigned()
                ->foreign('supplier_id')
                ->references('id')
                ->on('suppliers')
                ->nullable()                // <-- Nullable. Supplier can be changed.
            ;
            $table->timestamps();

            // A customer can only be on the waitlist for a specific car once.
            $table->unique(['car_id', 'customer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waitlists');
    }
}
