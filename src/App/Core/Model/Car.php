<?php

namespace App\Core\Model;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    const STATUS_DEVELOPMENT = 'development';
    const STATUS_PRODUCTION  = 'production';
}
