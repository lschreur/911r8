<?php

namespace App\Core\Model;

use Illuminate\Database\Eloquent\Model;

class Waitlist extends Model
{
    public function car()
    {
        return $this->belongsTo('App\Core\Model\Car');
    }

    public function color()
    {
        return $this->belongsTo('App\Core\Model\Color');
    }

    public function customer()
    {
        return $this->belongsTo('App\Core\Model\Customer');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Core\Model\Supplier');
    }
}
