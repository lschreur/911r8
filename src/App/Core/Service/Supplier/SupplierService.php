<?php

namespace App\Core\Service\Supplier;

use App\Core\Model\Supplier;

class SupplierService
{
    protected $repository;

    public function __construct(SupplierRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getById(int $id) : Supplier
    {
        $car = $this->repository->getById($id);

        if (empty($car)) {
            throw new Exception\SupplierNotFoundByIdException($id);
        }

        return $car;
    }

    // TODO: create, update, etc...
}
