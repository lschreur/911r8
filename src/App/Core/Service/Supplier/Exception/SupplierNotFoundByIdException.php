<?php

namespace App\Core\Service\Supplier\Exception;

class SupplierNotFoundByIdException extends SupplierNotFoundException
{
    public function __construct(int $id)
    {
        parent::__construct("The supplier with Id '{$id}' could not be found.");
    }
}
