<?php

namespace App\Core\Service\Supplier\Exception;

abstract class SupplierNotFoundException extends SupplierException
{
    
}
