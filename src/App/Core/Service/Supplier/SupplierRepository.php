<?php

namespace App\Core\Service\Supplier;

use App\Core\Model\Supplier;

class SupplierRepository
{
    public function getById(int $id) : ?Supplier
    {
        return Supplier::where('id', $id)->first();
    }
}
