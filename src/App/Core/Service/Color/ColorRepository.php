<?php

namespace App\Core\Service\Color;

use App\Core\Model\Color;

class ColorRepository
{
    public function getById(int $id) : ?Color
    {
        return Color::where('id', $id)->first();
    }
}
