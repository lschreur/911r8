<?php

namespace App\Core\Service\Color;

use App\Core\Model\Color;

class ColorService
{
    protected $repository;

    public function __construct(ColorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getById(int $id) : Color
    {
        $car = $this->repository->getById($id);

        if (empty($car)) {
            throw new Exception\ColorNotFoundByIdException($id);
        }

        return $car;
    }

    // TODO: create, update, etc...
}
