<?php

namespace App\Core\Service\Color\Exception;

abstract class ColorNotFoundException extends ColorException
{
    
}
