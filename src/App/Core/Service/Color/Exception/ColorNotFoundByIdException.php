<?php

namespace App\Core\Service\Color\Exception;

class ColorNotFoundByIdException extends ColorNotFoundException
{
    public function __construct(int $id)
    {
        parent::__construct("The color with Id '{$id}' could not be found.");
    }
}
