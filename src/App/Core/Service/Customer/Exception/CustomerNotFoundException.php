<?php

namespace App\Core\Service\Customer\Exception;

abstract class CustomerNotFoundException extends CustomerException
{
    
}
