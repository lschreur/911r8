<?php

namespace App\Core\Service\Customer\Exception;

class CustomerNotFoundByIdException extends CustomerNotFoundException
{
    public function __construct(int $id)
    {
        parent::__construct("The customer with Id '{$id}' could not be found.");
    }
}
