<?php

namespace App\Core\Service\Customer;

use App\Core\Model\Customer;

class CustomerService
{
    protected $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getById(int $id) : Customer
    {
        $car = $this->repository->getById($id);

        if (empty($car)) {
            throw new Exception\CustomerNotFoundByIdException($id);
        }

        return $car;
    }

    public function create(string $name) : Customer
    {
        $customer = new Customer();
        $customer->name = $name;
        $customer->save();

        return $customer;
    }
}
