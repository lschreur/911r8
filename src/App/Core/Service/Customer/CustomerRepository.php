<?php

namespace App\Core\Service\Customer;

use App\Core\Model\Customer;

class CustomerRepository
{
    public function getById(int $id) : ?Customer
    {
        return Customer::where('id', $id)->first();
    }
}
