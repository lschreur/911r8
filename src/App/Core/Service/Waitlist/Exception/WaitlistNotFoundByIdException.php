<?php

namespace App\Core\Service\Waitlist\Exception;

class WaitlistNotFoundByIdException extends WaitlistNotFoundException
{
    public function __construct(int $id)
    {
        parent::__construct("The waitlist with Id '{$id}' could not be found.");
    }
}
