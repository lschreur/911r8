<?php

namespace App\Core\Service\Waitlist\Exception;

use App\Core\Model\Car;
use App\Core\Model\Customer;

class WaitlistNotFoundByCarAndCustomerException extends WaitlistNotFoundException
{
    public function __construct(Car $car, Customer $customer)
    {
        parent::__construct("The waitlist for car '{$car->id}' and customer Id '{$customer->id}' could not be found.");
    }
}
