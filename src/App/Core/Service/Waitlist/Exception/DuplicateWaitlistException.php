<?php

namespace App\Core\Service\Waitlist\Exception;

use App\Core\Model\Car;
use App\Core\Model\Customer;

class DuplicateWaitlistException extends WaitlistException
{
    public function __construct(Car $car, Customer $customer)
    {
        parent::__construct("A waitlist for car Id '{$car->id}' and customer Id '{$customer->id}' already exists.");
    }
}
