<?php

namespace App\Core\Service\Waitlist;

use App\Core\Model\Car;
use App\Core\Model\Customer;
use App\Core\Model\Color;
use App\Core\Model\Supplier;
use App\Core\Model\Waitlist;

class WaitlistService
{
    protected $repository;

    public function __construct(WaitlistRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getById(int $id) : Waitlist
    {
        $waitlist = $this->repository->getById($id);

        if (empty($waitlist)) {
            throw new Exception\WaitlistNotFoundByIdException($id);
        }

        return $waitlist;
    }

    public function getAllByCar(Car $car)
    {
        return $this->repository->getAllByCar($car);
    }

    public function getByCarAndCustomer(Car $car, Customer $customer) : Waitlist
    {
        $waitlist = $this->repository->getByCarAndCustomer($car, $customer);

        if (empty($waitlist)) {
            throw new Exception\WaitlistNotFoundByCarAndCustomer($car, $customer);
        }

        return $waitlist;
    }

    public function create(Car $car, Customer $customer, Color $color = null, Supplier $supplier = null) : Waitlist
    {
        $existingWaitlist = $this->repository->getByCarAndCustomer($car, $customer);

        if (!empty($existingWaitlist)) {
            throw new Exception\DuplicateWaitlistException($car, $customer);
        }

        $waitlist = new Waitlist();
        $waitlist->car()->associate($car);
        $waitlist->customer()->associate($customer);

        if (!empty($color)) {
            $waitlist->color()->associate($color);
        }

        if (!empty($supplier)) {
            $waitlist->supplier()->associate($supplier);
        }

        $waitlist->save();

        return $waitlist;
    }

    public function update(Waitlist $waitlist, Color $color = null, Supplier $supplier = null) : Waitlist
    {
        if (!empty($color)) {
            $waitlist->color()->associate($color);
        }

        if (!empty($supplier)) {
            $waitlist->supplier()->associate($supplier);
        }

        $waitlist->save();

        return $waitlist;
    }

    public function delete(Waitlist $waitlist) : void
    {
        $waitlist->delete();
    }
}
