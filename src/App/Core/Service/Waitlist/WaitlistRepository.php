<?php

namespace App\Core\Service\Waitlist;

use App\Core\Model\Car;
use App\Core\Model\Customer;
use App\Core\Model\Waitlist;

class WaitlistRepository
{
    public function getById(int $id) : ?Waitlist
    {
        return Waitlist::where('id', $id)->first();
    }

    public function getAllByCar(Car $car)
    {
        return Waitlist::where('car_id', $car->id)->get();
    }

    public function getByCarAndCustomer(Car $car, Customer $customer) : ?Waitlist
    {
        $waitlist = Waitlist::where([
            'car_id' => $car->id,
            'customer_id' => $customer->id
        ])->first();

        return $waitlist;
    }
}
