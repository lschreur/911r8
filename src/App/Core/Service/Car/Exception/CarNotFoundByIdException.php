<?php

namespace App\Core\Service\Car\Exception;

class CarNotFoundByIdException extends CarNotFoundException
{
    public function __construct(int $id)
    {
        parent::__construct("The car with Id '{$id}' could not be found.");
    }
}
