<?php

namespace App\Core\Service\Car\Exception;

class CarNotFoundByNameException extends CarNotFoundException
{
    public function __construct(string $name)
    {
        parent::__construct("The car with name '{$name}' could not be found.");
    }
}
