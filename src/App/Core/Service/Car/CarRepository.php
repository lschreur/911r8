<?php

namespace App\Core\Service\Car;

use App\Core\Model\Car;

class CarRepository
{
    public function getById(int $id) : ?Car
    {
        return Car::where('id', $id)->first();
    }

    public function getByName(string $name) : ?Car
    {
        return Car::where('name', $name)->first();
    }
}
