<?php

namespace App\Core\Service\Car;

use App\Core\Model\Car;

class CarService
{
    protected $repository;

    public function __construct(CarRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getById(int $id) : Car
    {
        $car = $this->repository->getById($id);

        if (empty($car)) {
            throw new Exception\CarNotFoundByIdException($id);
        }

        return $car;
    }

    public function getByName(string $name) : Car
    {
        $car = $this->repository->getByName($name);

        if (empty($car)) {
            throw new Exception\CarNotFoundByNameException($name);
        }

        return $car;
    }

    // TODO: create, update, etc...
}
